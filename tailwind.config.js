module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: '1.5rem',
        sm: '2.5rem',
        md: '3.5rem',
        lg: '4.5rem',
        xl: '5.5rem',
        '2xl': '6.5rem',
      },
    },
    extend: {
      fontFamily: {
        poppins: 'Poppins, sans-serif',
        montserrat: '"Montserrat Alternates", sans-serif',
        'open-sans': '"Open Sans", sans-serif',
      },
      colors: {
        'dark-green': '#115B76',
        'light-green': '#1DC0AD',
        'light-green-hover': '#46e2d0',
        'ourwork-link': '#7C7C7C',
        'ourwork-description': '#3B3B3B',
        ourteam1: '#D3FFCC',
        ourteam2: '#E1E8FB',
        ourteam3: '#FFE8CD',
        'gray-light': '#D7D7D7',
      },
      height: {
        greeting: '450px',
        mobileGreeting: '200px',
        union: '50px',
        circle: '85px',
        ourteam: '360px',
        dot: '110px',
        'l-dot': '50px',
        footer: '450px',
        navbar: '15vh',
        navbarMobile: '10vh',
        hero: '85vh',
        heroMobile: '90vh',
      },
      width: {
        union: '50px',
        circle: '85px',
        ourteam: '360px',
        dot: '230px',
        'l-dot': '50px',
      },
      backgroundImage: {
        greeting: `url('/src/assets/images/map.svg')`,
        union: 'url("/src/assets/images/union.svg")',
        circle: 'url("/src/assets/images/circle.svg")',
        line: 'url("/src/assets/images/background-line.svg")',
        dot: 'url("/src/assets/images/dot.svg")',
        'l-dot': 'url("/src/assets/images/l-dot.svg")',
      },
      borderRadius: {
        web: '40px 40px 5px 40px',
        mobile: '30px 30px 5px 5px',
        ui: '40px 40px 40px 5px',
        machine: '40px 5px 40px 40px',
        automation: '5px 5px 40px 40px',
        branding: '5px 40px 40px 40px',
      },
      boxShadow: {
        service: '0px 8px 20px 1px rgba(0, 0, 0, 0.05)',
      },
      dropShadow: {
        'ourwork-left': '-49px 46px 70px rgba(0, 0, 0, 0.11)',
        'ourwork-right': '49px 46px 70px rgba(0, 0, 0, 0.11)',
        'ourwork-button': '0px 4px 4px rgba(0, 0, 0, 0.25)',
      },
      ringWidth: {
        15: '15px',
      },
      translate: {
        10.5: '-43px',
        8.5: '35px',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
