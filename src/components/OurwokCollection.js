import React from 'react'
import Image, { Shimmer } from 'react-image-shimmer'

const OurwokCollection = (props) => {
  return (
    <div className="grid grid-cols-12">
      {props.works.map((work, index) => (
        <div className="ourwork-collection col-span-12" key={index}>
          <div
            className={`ourwork-collection-image ${index % 2 === 0 ? `order-${index + 1}` : `order-${index + 2}`}`}
            data-aos={`${index % 2 === 0 ? `fade-right` : `fade-left`}`}
            data-aos-duration="1000"
            data-aos-delay="600"
          >
            <Image
              src={work.image}
              className={`filter ${index % 2 === 0 ? `drop-shadow-ourwork-left` : `drop-shadow-ourwork-right`}`}
              alt="Karya Kami 1"
              fallback={<Shimmer width={500} height={300} />}
            />
          </div>
          <div
            className={`ourwork-collection-text ${index % 2 === 0 ? `order-${index + 2}` : `order-${index + 1}`}`}
            data-aos={index % 2 === 0 ? `fade-left` : `fade-right`}
            data-aos-duration="1000"
            data-aos-delay="600"
          >
            <div className="union"></div>
            <h1>{work.title}</h1>
            <a href="/">{work.link}</a>
            <p>{work.desc}</p>
          </div>
        </div>
      ))}
    </div>
  )
}

export default OurwokCollection
