// Import Core Module
import { Link, useLocation } from 'react-router-dom'

// Import Thrid-Party Module
import { Disclosure, Transition } from '@headlessui/react'

// Import Image
import logo from '../assets/logo.png'

const navs = [
  { name: 'Home', to: '/' },
  { name: 'Pelayanan', to: '/pelayanan' },
  { name: 'Karya Kami', to: '/karya-kami' },
  { name: 'Harga', to: '/harga' },
  { name: 'Kontak', to: '/kontak' },
]

const Navbar = () => {
  const loc = useLocation()

  return (
    <Disclosure>
      {({ open }) => (
        <>
          <div className={`${open ? 'bg-white sm:bg-transparent' : 'bg-transparent'} navbar`}>
            <Link to="/">
              <img src={logo} alt="Logo Art By Code" />
            </Link>
            <Disclosure.Button className="inline-flex md:hidden p-2 shadow">
              {open ? <i className="fa fa-times fa-lg" /> : <i className="fa fa-bars fa-lg" />}
            </Disclosure.Button>
            <ul className="hidden md:flex text-dark-green font-normal text-lg font-poppins">
              {navs.map((nav) => (
                <li key={nav.name}>
                  <Link
                    className={`${
                      loc.pathname === nav.to ? 'text-light-green' : ''
                    } ml-6 hover:text-light-green duration-200`}
                    to={nav.to}
                  >
                    {nav.name}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
          <Transition
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
          >
            <Disclosure.Panel className="absolute w-full bg-white z-50 sm:hidden text-dark-green container pb-6 font-regular text-lg font-poppins">
              <ul>
                {navs.map((nav) => (
                  <li key={nav.name} className="my-2">
                    <Link
                      className={`${
                        loc.pathname === nav.to ? 'text-light-green' : ''
                      } hover:text-light-green duration-200`}
                      to={nav.to}
                    >
                      {nav.name}
                    </Link>
                  </li>
                ))}
              </ul>
            </Disclosure.Panel>
          </Transition>
        </>
      )}
    </Disclosure>
  )
}

export default Navbar
