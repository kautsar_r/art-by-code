import React from 'react'

// Import Images
import logo from '../assets/logo.png'

const Footer = () => {
  return (
    <div className="container py-20 grid grid-cols-12 md:justify-items-center items-stretch gap-6 md:gap-0">
      <div className="col-span-12 sm:col-span-5 place-self-start flex flex-col h-full justify-between">
        <div className="flex items-center">
          <img src={logo} alt="Logo Art By Code" className="w-20" />
          <h1 className="font-montserrat font-bold text-4xl text-light-green">Art By Code</h1>
        </div>
        <p>© Art By Code. 2021 All rights reserved</p>
      </div>
      <div className="col-span-12 sm:col-span-2">
        <h1 className="text-2xl">Fitur</h1>
        <ul>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Home</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Pelayanan</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Karya</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Harga</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Kontak</a>
          </li>
        </ul>
      </div>
      <div className="col-span-12 sm:col-span-3">
        <h1 className="text-2xl">Media Sosial</h1>
        <ul>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Instagram</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Whatsapp</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Twitter</a>
          </li>
        </ul>
      </div>
      <div className="col-span-12 sm:col-span-2">
        <h1 className="text-2xl">Useful Links</h1>
        <ul>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Privasi & policy</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Terms & Conditions</a>
          </li>
          <li className="mt-4 text-xl opacity-50 hover:opacity-100 duration-200">
            <a href="/">Jam Operasional</a>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default Footer
