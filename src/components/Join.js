const Join = () => {
  return (
    <div className="join">
      <div className="dot-top" data-aos="fade-left" data-aos-duration="800" data-aos-delay="600"></div>
      <h1 data-aos="zoom-in-up" data-aos-duration="1000" data-aos-delay="800">
        Segera Gabung, Yuk!
      </h1>
      <p data-aos="zoom-out" data-aos-duration="1000" data-aos-delay="1000">
        Mari bekerja sama dengan kami
      </p>
      <p data-aos="zoom-out" data-aos-duration="1000" data-aos-delay="1400">
        dan dapatkan pelayanan terbaik dari Art By Code 😊
      </p>
      <button data-aos="zoom-in-up" data-aos-duration="1000" data-aos-delay="1600">
        Kontak Kami
      </button>
      <div className="dot-bottom" data-aos="fade-left" data-aos-duration="800" data-aos-delay="600"></div>
    </div>
  )
}

export default Join
