import { useEffect, useState } from 'react'

const useScroll = () => {
  const [scrollPosition, setScrollPosition] = useState(null)

  const handleScroll = () => {
    setScrollPosition(window.scrollY)
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
  }, [scrollPosition])

  return { scrollPosition }
}

export default useScroll
