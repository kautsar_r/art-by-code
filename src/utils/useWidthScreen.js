import { useEffect, useState } from 'react'

const useWidthScreen = () => {
  const [widthScreen, setWidthScreen] = useState(null)

  const screenWidth = document.body.offsetWidth + 17

  useEffect(() => {
    window.addEventListener('resize', setWidthScreen(screenWidth))

    setWidthScreen(screenWidth)
  }, [screenWidth])

  return { widthScreen }
}

export default useWidthScreen
