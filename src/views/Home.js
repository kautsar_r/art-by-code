// Import Core Module
import { useEffect, useState } from 'react'

// Import Thrid-party Module
import Typed from 'typed.js'
import Image, { Shimmer } from 'react-image-shimmer'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

// Import Custom hooks
import useWidthScreen from '../utils/useWidthScreen'

// Import Components
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'

// Import Image
import hero from '../assets/images/hero.png'
import web from '../assets/icons/web-icon.svg'
import mobile from '../assets/icons/mobile-icon.svg'
import ui from '../assets/icons/ui-icon.svg'
import machine from '../assets/icons/machine-icon.svg'
import automation from '../assets/icons/automation-icon.svg'
import branding from '../assets/icons/branding-icon.svg'
import ourwork1 from '../assets/images/ourwork1.svg'
import ourwork2 from '../assets/images/ourwork2.svg'
import Join from '../components/Join'
import OurwokCollection from '../components/OurwokCollection'
import team from '../assets/images/teams.png'
import team1 from '../assets/images/team1.png'
import team2 from '../assets/images/team2.png'
import team3 from '../assets/images/team3.png'
import team4 from '../assets/images/team4.png'

const ourworks = [
  {
    title: 'Makes Easy Paper',
    link: 'https://loremipsum2.com',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis elit faucibus ornare porttitor tellus mauris proin nulla. Integer enim nisl purus pellentesque orci aliquam quam urna.',
    image: ourwork1,
  },
  {
    title: 'Makes Easy Paper',
    link: 'https://loremipsum2.com',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis elit faucibus ornare porttitor tellus mauris proin nulla. Integer enim nisl purus pellentesque orci aliquam quam urna.',
    image: ourwork2,
  },
]

const teams = [team1, team2, team3, team4]

const testimonial = [
  {
    name: 'Achmad Fauzi',
    position: 'Fullstack Developer',
    comment:
      'With Front Pay, you can check out across the web and in apps without having to enter any payment information.',
  },
  {
    name: 'Hafizh Maulana Y.',
    position: 'Fullstack Developer',
    comment:
      "From boarding passes to transit and movie tickets, there's pretty much nothing you can't store with Front Pay.",
  },
  {
    name: 'M. Kautsar. P',
    position: 'Fullstack Developer',
    comment:
      "I love Front Pay for cash back, reward points and fraud protection – just like when you're swiping your card.",
  },
  {
    name: 'Achmad Fauzi',
    position: 'Fullstack Developer',
    comment:
      'With Front Pay, you can check out across the web and in apps without having to enter any payment information.',
  },
  {
    name: 'Hafizh Maulana Y.',
    position: 'Fullstack Developer',
    comment:
      "From boarding passes to transit and movie tickets, there's pretty much nothing you can't store with Front Pay.",
  },
  {
    name: 'M. Kautsar. P',
    position: 'Fullstack Developer',
    comment:
      "I love Front Pay for cash back, reward points and fraud protection – just like when you're swiping your card.",
  },
]

let stars = []
for (let index = 1; index <= 5; index++) {
  stars.push(<i className="fa fa-star mx-1"></i>)
}

const Home = () => {
  const [mobileScreen, setMobileScreen] = useState(false)
  const { widthScreen } = useWidthScreen()
  const [left, setLeft] = useState(false)
  const [right, setRight] = useState(false)

  const next = () => {
    setLeft(false)
    setRight(true)
  }

  const prev = () => {
    setLeft(true)
    setRight(false)
  }

  function SampleNextArrow(props) {
    const { onClick } = props
    return (
      <div
        className={`slider-next ${right ? 'bg-light-green text-white' : 'bg-white text-light-green'}`}
        onClick={() => {
          onClick()
          next()
        }}
      >
        <i className="fa fa-chevron-right" />
      </div>
    )
  }

  function SamplePrevArrow(props) {
    const { onClick } = props
    return (
      <div
        className={`slider-prev ${left ? 'bg-light-green text-white' : 'bg-white text-light-green'}`}
        onClick={() => {
          onClick()
          prev()
        }}
      >
        <i className="fa fa-chevron-left" />
      </div>
    )
  }

  const settings = {
    infinite: true,
    speed: 1000,
    slidesToShow: 2,
    slidesToScroll: 2,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  }

  useEffect(() => {
    let typed = new Typed(document.getElementById('title'), {
      strings: ['Design ^1000', 'Line ^1200', 'Code ^100'],
      typeSpeed: 70,
      backSpeed: 60,
    })
  }, [])

  useEffect(() => {
    widthScreen <= 768 ? setMobileScreen(true) : setMobileScreen(false)
  }, [widthScreen])

  return (
    <div className="home">
      <Navbar />
      {/* Hero Section */}
      <div className="hero">
        <div className="hero-text" data-aos="fade-right" data-aos-duration="1000">
          <div className="hero-text-title">
            <h1>Hai, kami adalah</h1>
            <h1>
              Art By <span id="title"></span> ABC
            </h1>
          </div>
          <p>Kita bisa menciptakan sesuatu yang indah dari kode</p>
          <div className="hero-text-button">
            <button>Kontak Kami</button>
            <button>Lihat Produk</button>
          </div>
        </div>
        <div className="hero-image" data-aos="fade-left" data-aos-duration="1000">
          <Image src={hero} alt="Hero" fallback={<Shimmer width={500} height={400} />} />
        </div>
      </div>
      {/* Greeting Section */}
      <div className="greeting">
        <div className="greeting-content">
          <h1 data-aos="fade-up" data-aos-duration="1200">
            Kenalan Yuk!
          </h1>
          <div className="greeting-content-description">
            <p data-aos="fade-right" data-aos-duration="1000" data-aos-delay="800">
              Kami adalah <span>Art by Code</span>, sebuah Tim yang bisa melayani kebutuhanmu dalam dunia digital saat
              ini. Di saat kemajuan teknologi semakin pesat, pastikan kebutuhanmu terpenuhi dengan tepat.
            </p>
            <p data-aos="fade-left" data-aos-duration="1000" data-aos-delay="1200">
              Intinya, kami adalah Tim yang akan membantumu bekerja dan menciptakan karya dengan membuat website keren,
              disertai tampilan desain yang modern
            </p>
          </div>
        </div>
      </div>
      {/* Service Section */}
      <div className="service">
        <h1>Semua Layanan yang kami berikan untukmu</h1>
        <div className="service-collection" data-aos="zoom-in" data-aos-duration="1400">
          <div className="service-bg-circle-top"></div>
          <div
            className="service-collection-web"
            data-aos={`${mobileScreen ? 'fade-up' : 'fade-down-right'}`}
            data-aos-duration="1400"
            data-aos-delay="600"
          >
            <img src={web} alt="Web Icon" />
            <h1>Web Development</h1>
            <p>Situs web yang dibuat khusus dan responsif. kelola konten seperti anda inginkan</p>
          </div>
          <div
            className="service-collection-mobile"
            data-aos={`${mobileScreen ? 'fade-up' : 'fade-down'}`}
            data-aos-duration="1400"
            data-aos-delay="600"
          >
            <img src={mobile} alt="Mobile Icon" />
            <h1>Mobile Development</h1>
            <p>Pembuatan aplikasi cross platform dengan teknologi masa kini</p>
          </div>
          <div
            className="service-collection-ui"
            data-aos={`${mobileScreen ? 'fade-up' : 'fade-down-left'}`}
            data-aos-duration="1400"
            data-aos-delay="600"
          >
            <img src={ui} alt="Ui UX Icon" />
            <h1>UI / UX Design</h1>
            <p>Punya ide bagus ? biarkan kami membuat dan mengembangkan design yang kamu inginkan</p>
          </div>
          <div
            className="service-collection-machine"
            data-aos={`${mobileScreen ? 'fade-up' : 'fade-up-right'}`}
            data-aos-duration="1400"
            data-aos-delay="600"
          >
            <img src={machine} alt="Machine Learning Icon" />
            <h1>Machine Learning</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <div
            className="service-collection-automation"
            data-aos={`${mobileScreen ? 'fade-up' : 'fade-up'}`}
            data-aos-duration="1400"
            data-aos-delay="600"
          >
            <img src={automation} alt="Automation Icon" />
            <h1>Automation System</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <div
            className="service-collection-branding"
            data-aos={`${mobileScreen ? 'fade-up' : 'fade-up-left'}`}
            data-aos-duration="1400"
            data-aos-delay="600"
          >
            <img src={branding} alt="Branding Icon" />
            <h1> Branding & Digital Ads</h1>
            <p>Capai Tujuan Anda Melalui Otomatisasi Pemasaran, SEO, Komunitas, dll</p>
          </div>
          <div className="service-bg-circle-bottom" data-aos="zoom-in" data-aos-duration="1400"></div>
        </div>
      </div>
      {/* Ourwork Section */}
      <div className="ourwork">
        <h1 className="ourwork-title">Karya Kami</h1>
        <OurwokCollection works={ourworks} />
        <button className="ourwork-button" data-aos="fade-up" data-aos-duration="1200" data-aos-delay="1000">
          Karya Lainnya
        </button>
      </div>
      {/* Ourteam Section */}
      <div className="container my-12 sm:my-32 grid grid-cols-12 items-center">
        <div className="col-span-12 md:col-span-6 team">
          <img src={team} className="block md:hidden w-full h-full mb-6" alt="Team" />
          {teams.map((team, index) => (
            <img key={index} src={team} className={`team-${index + 1} duration-700`} alt={`Team ${index + 1}`} />
          ))}
        </div>
        <div className="col-span-12 md:col-span-6">
          <h2 className="font-semibold text-xl md:text-3xl text-dark-green opacity-50">Tim Kami</h2>
          <h1 className="font-semibold text-2xl md:text-4xl text-dark-green mt-2 md:mt-3 mb-5 md:mb-6">
            Lorem ipsum dolor sit amet adipiscing elit.
          </h1>
          <p className="text-sm md:text-base">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis elit faucibus ornare porttitor tellus mauris
            proin nulla.
          </p>
          <button className="py-2 px-6 rounded-full duration-200 bg-light-green hover:bg-light-green-hover filter drop-shadow-lg text-white mt-10 md:mt-16">
            Anggota Lainnya
          </button>
        </div>
      </div>
      <div className="section-testimonial">
        <div className="testimonial-heading">
          <h2>Pelanggan Kami</h2>
          <h1>Ulasan Pelanggan Kami Memilih Art By Code </h1>
        </div>
        <div className="testimonial-content">
          <div
            className="absolute w-12 h-12 -bottom-0 left-2 rounded-full -z-999"
            style={{ backgroundColor: '#F0673F' }}
          />
          <div className="absolute w-7 h-7 top-0 left-36 rounded-full -z-999" style={{ backgroundColor: '#C285EF' }} />
          <div
            className="absolute w-6 h-6 bottom-0 left-72 rounded-full -z-999"
            style={{ backgroundColor: '#E407F8' }}
          />
          <Slider {...settings} className="">
            {testimonial.map((test, index) => (
              <div className="px-6 py-4">
                <div className="testimonial">
                  <img
                    src="https://picsum.photos/seed/picsum/70"
                    alt={`Testimoni ${index + 1}`}
                    className="rounded-full mb-5 mx-auto"
                  />
                  <p className="opacity-50">{test.comment}</p>
                  <div className="text-sm text-light-green my-3">{stars}</div>
                  <h1 className="font-semibold text-xl mb-1">{test.name}</h1>
                  <p className="text-xs opacity-50">{test.position}</p>
                </div>
              </div>
            ))}
          </Slider>
          <div
            className="absolute w-12 h-12 top-0 right-2 rounded-full -z-999"
            style={{ backgroundColor: '#FFDC5D' }}
          />
          <div
            className="absolute w-8 h-8 bottom-6 right-2 rounded-full -z-999"
            style={{ backgroundColor: '#8AFAB7' }}
          />
          <div
            className="absolute w-6 h-6 -bottom-0 right-80 rounded-full -z-999"
            style={{ backgroundColor: '#FFF504' }}
          />
        </div>
      </div>
      <Join />
      <Footer />
    </div>
  )
}

export default Home
