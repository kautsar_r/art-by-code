import React from 'react'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'

const contents = [
  { name: 'Lorem ipsum lorem ipsum' },
  { name: 'Lorem ipsum lorem ipsum' },
  { name: 'Lorem ipsum lorem ipsum' },
  { name: 'Lorem ipsum lorem ipsum' },
  { name: 'Lorem ipsum lorem ipsum' },
  { name: 'Lorem ipsum lorem ipsum' },
]

const pricings = [
  {
    title: 'Basic',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    price: 'Rp. 3.500.000',
  },
  {
    title: 'Premium',
    desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    price: 'Rp. 7.500.000',
  },
  {
    title: 'Enterprise',
    desc: 'Hubungi lebih lanjut untuk informasi lainya',
    price: 'Harga Custom',
  },
]

const Pricing = () => {
  return (
    <div className="pricing">
      <Navbar />
      <div className="ourwork-heading relative px-6 md:px-20 w-3/4 sm:mt-20 py-14 mx-auto text-center font-poppins">
        <div className="w-l-dot h-l-dot absolute transform rotate-180 top-0 left-0 bg-l-dot hidden sm:block"></div>
        <h1 className="font-semibold text-xl sm:text-4xl text-dark-green mb-4 leading-snug">
          Simpan uangmu dan <br /> mari kerja sama denganku
        </h1>
        <p style={{ color: '#696969' }} className="text-base sm:text-xl">
          Kami mempunyai harga yang sangat terjangkau lohh <br /> untuk kamu
        </p>
        <div className="w-l-dot h-l-dot absolute bottom-0 right-0 bg-l-dot hidden sm:block"></div>
      </div>
      <div className="my-32 grid grid-cols-12 container items-center justify-between gap-6 sm:gap-20">
        {pricings.map((pricing, index) => (
          <div
            className={`${
              index === 1
                ? 'bg-dark-green ring-dark-green ring-opacity-25 relative overflow-hidden text-white'
                : 'bg-white ring-gray-light text-dark-green'
            } col-span-12 sm:col-span-4 ring-15 rounded-xl mb-6 sm:mb-0`}
          >
            <div className="text-center">
              <h1 className="mt-11 text-4xl font-bold">{pricing.title}</h1>
              <p className="text-sm my-5 leading-snug">{pricing.desc}</p>
              <p className="font-semibold text-3xl">{pricing.price}</p>
              <hr className={`${index === 1 ? 'border-white' : 'border-dark-green'} w-3/5 mx-auto mt-5 mb-14`} />
            </div>
            <div className="px-10 flex justify-center">
              <ul>
                {contents.map((content, index) => (
                  <li className="mb-5">
                    {index > 1 ? (
                      <div>
                        <i
                          className="far fa-lock rounded-full p-2"
                          style={{ backgroundColor: '#F6DEDE', color: '#D92929' }}
                        />
                        <p className="inline-block ml-4 opacity-50 truncate">{content.name}</p>
                      </div>
                    ) : (
                      <div className="truncate">
                        <i
                          className="fa fa-check rounded-full p-2"
                          style={{ backgroundColor: '#D7D7D7', color: '#22C58B' }}
                        />
                        <p className="inline-block ml-4">{content.name}</p>
                      </div>
                    )}
                  </li>
                ))}
              </ul>
            </div>
            <button
              className={`${
                index === 1 ? 'bg-white text-dark-green' : 'bg-dark-green text-white'
              } py-4 px-20 mx-auto block my-16 border font-semibold rounded-xl`}
            >
              Pilih
            </button>
          </div>
        ))}
      </div>
      <Footer />
    </div>
  )
}

export default Pricing
