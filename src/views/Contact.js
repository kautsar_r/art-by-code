import React from 'react'

// Import Component
import Navbar from '../components/Navbar'

// Import Images
import ContactBg from '../assets/images/contact-bg.svg'

const Contact = () => {
  return (
    <div className="contact-page">
      <Navbar />
      <div className="contact-page-content">
        <div className="contact-page-content-image" data-aos="zoom-out" data-aos-duration="1000" data-aos-delay="600">
          <img src={ContactBg} alt="Background Contact" />
        </div>
        <div className="contact-page-content-form">
          <h1 data-aos="fade-right" data-aos-duration="1000" data-aos-delay="800">
            Mari Kita Membangun Bersama
          </h1>
          <form data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1000">
            <div className="contact-page-content-form-input">
              <label>Nama</label>
              <input type="text" placeholder="Tulis Nama Kamu Disini" />
            </div>
            <div className="contact-page-content-form-input">
              <label>Email</label>
              <input type="text" placeholder="contoh@gmail.com" />
            </div>
            <div className="contact-page-content-form-textarea">
              <label>Pesan</label>
              <textarea placeholder="Tulis pesan kamu disini"></textarea>
            </div>
            <div className="contact-page-content-form-button">
              <button>Kirim</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default Contact
