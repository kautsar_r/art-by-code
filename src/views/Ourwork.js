import Footer from '../components/Footer'
import Join from '../components/Join'
import Navbar from '../components/Navbar'

const buttons = [
  { name: 'Semua', active: true },
  { name: 'Situs Web', active: false },
  { name: 'Mobile App', active: false },
  { name: 'Machine Learning', active: false },
  { name: 'UI / UX Design', active: false },
]

const Ourwork = () => {
  return (
    <div className="ourwork">
      <Navbar />
      <div className="ourwork-heading relative container w-3/4 sm:mt-20 py-14 mx-auto text-center font-poppins">
        <div className="w-l-dot h-l-dot absolute transform rotate-180 top-0 left-0 bg-l-dot"></div>
        <h1 className="font-semibold text-2xl sm:text-5xl text-dark-green mb-4">Karya Kami</h1>
        <p style={{ color: '#696969' }} className="text-sm sm:text-xl">
          Seluruh tim Art By Code terlibat dalam setiap proyek ambisius kliennya: mulai dari pembuatan situs web hingga
          pembuatan mobile app, termasuk pemikiran kami dalam pemasaran digital. Jelajahi portofolio kami dengan
          beberapa proyek luar biasa kami.
        </p>
        <div className="w-l-dot h-l-dot absolute bottom-0 right-0 bg-l-dot"></div>
      </div>

      <div className="ourwork-button flex flex-wrap items-center justify-center lg:justify-between w-3/4 my-32 mx-auto font-poppins font-medium text-base">
        {buttons.map((button) => (
          <button
            className={`${
              button.active
                ? 'bg-light-green text-white'
                : 'bg-white hover:bg-light-green text-light-green hover:text-white'
            } py-2 px-6 sm:px-12 text-xs sm:text-base rounded-full duration-200`}
          >
            {button.name}
          </button>
        ))}
      </div>

      <Join />
      <Footer />
    </div>
  )
}

export default Ourwork
