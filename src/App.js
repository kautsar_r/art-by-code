import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import AOS from 'aos'
import 'aos/dist/aos.css'

// Import Component
import Home from './views/Home'
import Contact from './views/Contact'
import Ourwork from './views/Ourwork'
import Pricing from './views/Pricing'

function App() {
  AOS.init()

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/kontak">
          <Contact />
        </Route>
        <Route exact path="/karya-kami">
          <Ourwork />
        </Route>
        <Route exact path="/harga">
          <Pricing />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
